//Name: Flavio Palacios Tinoco
//CS4350-001-Unix Systems Programming
//Assignment Number: 5
//Due Date: 11/01/2021

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

int main()
{
    char fileScout;  //Used to traverse file characters.
    char lineStorage[26];  //Stores a read file line.
    int lineNumbStorage[5];  //Stores file line numbers.
    char programming[11]="Programming";
    //bool arraysMatch=false;  //Used for the operation in "File4.txt".
    int counter=0;  //Accumulator.
    int remainder;
    FILE *filePointer; //Only used in task #1.
    FILE *targetFile; //Pointer for the original file created.
    FILE *fileTwo;  //File pointer with inversed alphabet characters.
    FILE *fileThree;
    FILE *fileFour;

    printf("This is a program designed to experiment with file operations!\n");
    printf("--------------------------------------------------------------\n\n");

    //TASK1:
    //- - - - - - - - - -
    printf("1. Contents of \"File1.txt\".\n\n");
    filePointer=fopen("File1.txt", "r");  //"filePointer" points to NULL.
    if(filePointer==NULL){
       printf("Desired file does not exist!");
    }
    fclose(filePointer);  //File closed.

    //TASK2:
    //- - - - - - - - - -
    printf("2. \"File1.txt\" is created.\n\n");
    targetFile=fopen("File1.txt", "w"); //Empty "File1.txt" created.
    fclose(targetFile);  //"File1.txt" closed.

    //TASK3:
    //- - - - - - - - - -
    printf("3. \"File1.txt\" is opened.\n\n");
    targetFile=fopen("File1.txt", "w+"); //"File1.txt" opened for writing.

    //TASK4:
    //- - - - - - - - - -
    printf("4. Data written inside \"File1.txt\".\n\n");
    targetFile=fopen("File1.txt", "w+");
    fprintf(targetFile, "CS 4350 - 251\n");  //Data is written.
    fprintf(targetFile, "Unix Programming;\n");
    fprintf(targetFile, "Fall 2021\n");
    fprintf(targetFile, "Programming #5;\n");
    fprintf(targetFile, "Due Date November 1, 2021");

    //TASK5:
    //- - - - - - - - - -
    printf("5. \"File1.txt\" is closed and reopened.\n\n");
    fclose(targetFile);  //"File1.txt" is closed.
    targetFile=fopen("File1.txt", "r");  //"File1.txt" opened for reading.

    //TASK6:
    //- - - - - - - - - -
    printf("6. \"File1.txt\" contents:\n\n");
    fileScout=fgetc(targetFile);
    while(fileScout!=EOF){  //Every character is traversed until "EOF" is reached.
       printf("%c", fileScout);
       fileScout=fgetc(targetFile);
    }
    fclose(targetFile);  //"File1.txt" is closed.
    targetFile=fopen("File1.txt", "r");  //"File1.txt" opened for reading.

    //TASK7:
    //- - - - - - - - - -
    printf("\n\n7. Number of lines in \"File1.txt\": ");
    fileScout=fgetc(targetFile);
    while(fileScout!=EOF){  //Every character is traversed until "EOF" is reached.
       if(fileScout=='\n') counter++;  //A found newspace increases my counter by 1.
       fileScout=fgetc(targetFile);
    }
    counter++;  //"EOF" is not accounted for. So I account for it this way.
    printf("%i", counter);  //The number of lines.
    printf("\n\n");
    counter=0;  //Counter is reset.
    fclose(targetFile);  //"File1.txt" is closed.
    targetFile=fopen("File1.txt", "r");  //"File1.txt" opened for reading.

    //TASK8:
    //- - - - - - - - - -
    printf("8. Number of words in \"File1.txt\": ");
    fileScout=fgetc(targetFile);
    while(fileScout!=EOF){  //File is traversed until a newline or space is hit.
       if((fileScout=='\n')||(fileScout==' ')) counter++; //"counter" keeps track
       fileScout=fgetc(targetFile);                       //spaces and newlines.
    }
    counter++;  //"counter" misseses the final word due to "EOF". Here is my fix.
    printf("%i", counter); //The number of words.
    printf("\n\n");
    counter=0; //Counter is reset.
    fclose(targetFile);  //"File1.txt" is closed.
    targetFile=fopen("File1.txt", "r"); //"File1.txt" opened for reading.

    //TASK9:
    //- - - - - - - - - -
    printf("9. Number of small letters in \"File1.txt\": ");
    fileScout=fgetc(targetFile);
    while(fileScout!=EOF){  //Every character is traversed until "EOF" is reached.
        if((fileScout>=97)&&(fileScout<=122))counter++; //Counts number of
        fileScout=fgetc(targetFile);                    //small letters.
    }
    printf("%i", counter); //The number of letters.
    printf("\n\n");
    counter=0; //Counter is reset.
    fclose(targetFile);  //"File1.txt" is closed.

    //TASK10:
    //- - - - - - - - - -
    printf("10. Number of capital letters in \"File1.txt\": ");
    targetFile=fopen("File1.txt", "r"); //"File1.txt" opened for reading.
    fileScout=fgetc(targetFile);
    while(fileScout!=EOF){  //Every character is traversed until "EOF" is reached.
        if((fileScout>=65)&&(fileScout<=90))counter++; //Counts number of
        fileScout=fgetc(targetFile);                    //big letters.
    }
    printf("%i", counter); //The number of letters.
    printf("\n\n");
    counter=0; //Counter is reset.
    fclose(targetFile);  //"File1.txt" is closed.


    //TASK11:
    //- - - - - - - - - -
    printf("11. Number of digits in \"File1.txt\": ");
    targetFile=fopen("File1.txt", "r"); //"File1.txt" opened for reading.
    fileScout=fgetc(targetFile);
    while(fileScout!=EOF){  //Every character is traversed until "EOF" is reached.
        if((fileScout>=48)&&(fileScout<=57))counter++; //Counts number of
        fileScout=fgetc(targetFile);                    //digits.
    }
    printf("%i", counter); //The number of digits.
    printf("\n\n");
    counter=0; //Counter is reset.
    fclose(targetFile);  //"File1.txt" is closed.

    //TASK12:
    //- - - - - - - - - -
    printf("12. Alphanumeric characters in \"File1.txt\": ");
    targetFile=fopen("File1.txt", "r"); //"File1.txt" opened for reading.
    fileScout=fgetc(targetFile);
    while(fileScout!=EOF){  //Every character is traversed until "EOF" is reached.
        if(((fileScout>=48)&&(fileScout<=57))||((fileScout>=65)&&(fileScout<=90))||
           ((fileScout>=97)&&(fileScout<=122))) {counter++;}  //Counts number of
        fileScout=fgetc(targetFile);                          //letters and numbers.
    }
    printf("%i", counter); //The number of characters.
    printf("\n\n");
    counter=0; //Counter is reset.
    fclose(targetFile);  //"File1.txt" is closed.

    //TASK13:
    //- - - - - - - - - -
    printf("13. Hexadecimal characters in \"File1.txt\": ");
    targetFile=fopen("File1.txt", "r"); //"File1.txt" opened for reading.
    fileScout=fgetc(targetFile);
    while(fileScout!=EOF){  //Every character is traversed until "EOF" is reached.
       if(((fileScout%15)>=10)&&((fileScout%15)<=15)) {counter++;}  //Isolated chars here
       fileScout=fgetc(targetFile);                                 //have the hexadecimal
    }                                                               //elements of A,B,C,D,E,or F.
    printf("%i", counter); //The number of characters.
    printf("\n\n");
    counter=0; //Counter is reset.
    fclose(targetFile);  //"File1.txt" is closed.

    //TASK14:
    //- - - - - - - - - -
    printf("14. Number of spaces in \"File1.txt\": ");
    targetFile=fopen("File1.txt", "r"); //"File1.txt" opened for reading.
    fileScout=fgetc(targetFile);
    while(fileScout!=EOF){  //Every character is traversed until "EOF" is reached.
       if(fileScout==32){counter++;}  //Counts number of spaces
       fileScout=fgetc(targetFile);
    }
    printf("%i", counter); //The number of space characters.
    printf("\n\n");
    counter=0; //Counter is reset.
    fclose(targetFile);  //"File1.txt" is closed.

    //TASK15:
    //- - - - - - - - - -
    printf("15. Spcecial characters in \"File1.txt\": ");
    targetFile=fopen("File1.txt", "r"); //"File1.txt" opened for reading.
    fileScout=fgetc(targetFile);
    while(fileScout!=EOF){  //Every character is traversed until "EOF" is reached.
       if(((fileScout>=33)&&(fileScout<=47))||((fileScout>=58)&&(fileScout<=64))){
          counter++;}  //Counts number of special characters in this case.
       fileScout=fgetc(targetFile);
    }
    printf("%i", counter); //The number of characters.
    printf("\n\n");
    counter=0; //Counter is reset.
    fclose(targetFile);  //"File1.txt" is closed.

    //TASK18:
    //- - - - - - - - - -
    printf("18. Data written inside \"File2.txt\".\n\n");
    targetFile=fopen("File1.txt", "r");  //"File1.txt" is opened for reading.
    fileTwo=fopen("File2.txt", "w+");  //"File2.txt" is created.
    fileScout=fgetc(targetFile);
    while(fileScout!=EOF){   //Every character is traversed in the file.
       if(isalpha(fileScout)&&(fileScout<=90)){
          fileScout=fileScout+32;  //Capital letters are now lowercase.
          fputc(fileScout, fileTwo);
       }
       else if(isalpha(fileScout)&&(fileScout>=97)){
          fileScout=fileScout-32;  //Lowercase letters are now uppercase.
          fputc(fileScout, fileTwo);
       }
       else fputc(fileScout, fileTwo);  //A non-alphabet character is found.
       fileScout=fgetc(targetFile);
    }
    fclose(targetFile);  //"File1.txt" is closed.
    fclose(fileTwo);  //"File2.txt" is closed.

    //TASK19:
    //- - - - - - - - - -
    printf("19. \"File2.txt\" contents:\n\n");
    fileTwo=fopen("File2.txt", "r");
    fileScout=fgetc(fileTwo);
    while(fileScout!=EOF){  //Every character is traversed until "EOF" is reached.
       printf("%c", fileScout);
       fileScout=fgetc(fileTwo);
    }
    fclose(fileTwo);  //"File2.txt" is closed.

    //TASK20:
    //- - - - - - - - - -
    printf("\n\n20. Data written inside \"File3.txt\".\n\n");
    targetFile=fopen("File1.txt", "r");  //"File1.txt" is opened for reading.
    fileThree=fopen("File3.txt", "w+");  //"File3.txt" is created.
    counter=1;
    while(counter!=6){  //Loops 5 times which equals the number of lines in this file.
       fgets(lineStorage, 26, targetFile);  //Reads one file line at a time.
       fprintf(fileThree, "*** ");
       fprintf(fileThree, "%d", counter);
       fprintf(fileThree, " - ");
       fprintf(fileThree, lineStorage);
       counter++;
    }
    counter=0;  //Counter is reset.
    fclose(targetFile);  //"File1.txt" is closed.
    fclose(fileThree);  //"File3.txt" is closed.

    //TASK21:
    //- - - - - - - - - -
    printf("21. \"File3.txt\" contents:\n\n");
    fileThree=fopen("File3.txt", "r");
    fileScout=fgetc(fileThree);
    while(fileScout!=EOF){  //Every character is traversed until "EOF" is reached.
       printf("%c", fileScout);
       fileScout=fgetc(fileThree);
    }
    fclose(fileThree);  //"File3.txt" is closed.

    //TASK22:
    //- - - - - - - - - -
    printf("\n\n22. \"File4.txt\" created and the word \"Programming\" is changed.\n\n");
    targetFile=fopen("File1.txt", "r");  //"File1.txt" is opened for reading.
    fileFour=fopen("File4.txt", "w+");  //"File4.txt" is created.
    while(fscanf(targetFile, "%s", lineStorage)==1){
        if(strcmp(lineStorage, programming)==1){
            fprintf(fileFour, " Assignment ");
        }
        else fprintf(fileFour, lineStorage);
    }
    fclose(targetFile);  //"File1.txt" is closed.
    fclose(fileFour);  //"File4.txt" is closed.

    //TASK23:
    //- - - - - - - - - -
    printf("23. \"File4.txt\" contents:\n\n");
    fileFour=fopen("File4.txt", "r");
    fileScout=fgetc(fileFour);
    while(fileScout!=EOF){  //Every character is traversed until "EOF" is reached.
       printf("%c", fileScout);
       fileScout=fgetc(fileFour);
    }
    fclose(fileFour);  //"File4.txt" is closed.

    printf("\n\nImplemented By Flavio PalaciosTinoco\n");
    printf("11-01-2021");
    return 0;
}
